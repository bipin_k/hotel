<?php
class NewsController extends CI_Controller
{
	public function index()
	{
		$this->load->helper('url');
		$this->load->view('templates/header');
		$this->load->view('pages/news.php');
		$this->load->view('templates/footer');
	}
}
?>