<div id="myCarousel" class="carousel slide" data-ride="carousel">
	<!-- Indicators -->
	<ol class="carousel-indicators">
		<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
		<li data-target="#myCarousel" data-slide-to="1"></li>
		<li data-target="#myCarousel" data-slide-to="2"></li>
		<li data-target="#myCarousel" data-slide-to="3"></li>
		<li data-target="#myCarousel" data-slide-to="4"></li>
	</ol>

	<!-- Wrapper for slides -->
	<div class="carousel-inner">
		<div class="item active">
			<img src="/assets/images/image1.jpeg" alt="Image 1" style="width:100%; height:350px;">
		</div>

		<div class="item">
			<img src="/assets/images/image3.jpeg" alt="Image 2" style="width:100%; height:350px;">
		</div>

		<div class="item">
			<img src="/assets/images/image2.jpeg" alt="Image 3" style="width:100%; height:350px;">
		</div>
		<div class="item">
			<img src="/assets/images/la.jpeg" alt="Image 3" style="width:100%; height:350px;">
		</div>
		<div class="item">
			<img src="/assets/images/image5.jpeg" alt="Image 3" style="width:100%; height:350px;">
		</div>
	</div>

	<!-- Left and right controls -->
	<a class="left carousel-control" href="#myCarousel" data-slide="prev">
		<span class="glyphicon glyphicon-chevron-left" style="margin-left:-10px"></span>
		<span class="sr-only">Previous</span>
	</a>
	<a class="right carousel-control" href="#myCarousel" data-slide="next">
		<span class="glyphicon glyphicon-chevron-right" style="margin-right:-10px"></span>
		<span class="sr-only">Next</span>
	</a>
</div>
<br>
<div class= "container-fluid">
	<br>

	<p>At Hotel Shivaay Residency, Khalilabad, everyone is welcome. Whether you’re on a trip with family, off to a business meeting or planning any ceremony including wedding, birthday party or any event, experience the joy of hospitality in a fun, comfortable and affordable hotel.</p>
	<p>At Hotel Shivaay Residency, hospitality is more personal. A welcoming smile, breakfast, lunch & dinner on a real plate, a complimentary Wi-Fi connection to those you love. Enjoy the comforts that make you feel like family.</p>
	<p>We are located on NH28 across Gorakhpur-Lucknow National Highway, merely at a distance of 2 minutes from Khalilabad Bus Stand and 10 minutes from Khalilabad Railway Station.</p>
	<p>We offer Deluxe, Double & Single Air-Conditioned Rooms along with Banquet Hall and fine dining experience across day. We have a range of cuisines varying from North Indian, South Indian, Chinese and special dishes from our state, Uttar Pradesh.</p>
	<p>No other day in your life compares to your wedding day or any special event, be it family or official. From the big fat Indian weddings to a small engagement ceremonies and any business event, we'll ensure your big day is full of celebration and revelry. Photography, Catering and Music has got everything covered. And if that's not enough, you'll also have a dedicated Event Manager to attend to all your needs. Enjoy the journey to your bliss and leave the planning to us! </p>
	<p>
		We offer following services including,
		<ul>
			<li>Deluxe, Double & Single Air-Conditioned Rooms,</li>
			<li>Banquet Hall,</li>
			<li>In-house Restaurant,</li>
			<li>Security with CCTV Cameras,</li>
			<li>Power Backup,</li>
			<li>TV, Geyser and Mini Fridge,</li>
			<li>Parking & Laundry Facility</li>
		</ul>
	</p>
</div>
