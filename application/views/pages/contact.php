<div class= "container-fluid">
  <br>
  <div class="row">
    <div class="col-sm-6">
      <b>Please contact us for any bookings request:</b><br><br>
        <?php echo form_open('Pages/feedback_submit', 'class="form-horizontal" id="feedback"'); ?>
        <div class="form-group">
          <label class="control-label col-sm-2" for="name">Name:</label>
          <div class="col-sm-6">
            <input type="text" required class="form-control" id="name" placeholder="Enter name!" name="name" >
            <?php echo form_error('name'); ?>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-sm-2" for="email">Email:</label>
          <div class="col-sm-6">          
            <input type="email" required class="form-control" id="email" placeholder="Enter email!" name="email" >
            <?php echo form_error('email'); ?>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-sm-2" for="phone">Phone:</label>
          <div class="col-sm-6">          
            <input type="number" required class="form-control" id="phone" placeholder="Enter phone number!" name="phone" >
            <?php echo form_error('phone'); ?>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-sm-2" for="comment">Comment:</label>
          <div class="col-sm-6">          
            <textarea class="form-control" required id="comment" placeholder="Enter your query!" name="comment" rows="5" autocomplete="autocomplete_off_hack_xfr4!k"></textarea>
            <?php echo form_error('comment'); ?>
          </div>
        </div>
        <div class="form-group">        
          <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-success">Submit</button>
            <button type="reset" class="btn btn-default">Reset</button>
          </div>
        </div>
      </form>
      <br>
      <br>
    </div>
    <div class="col-sm-6">
      <p>
        <b>Please contact us on following address:</b>
        <address>
          <b>Hotel Shivaay Residency,</b><br>
          NH28, Near Jalkal Road,<br>
          Khalilabad, Sant Kabir Nagar<br>
          Uttar Pradesh - 272175<br>
        </address>
        <br>
      </p>

      <p>
        Please feel free to email us in case of any query:
        <a href="mailto:hotelshivaayresidencykld@gmail.com?Subject=Request%20For%20Hotel%20Bookings" target="_top">Send Mail</a><br><br>
        Also, you can contact us on following Mobile Number, <b>+91-9151260784</b><br>
      </p>

      <p>
        You can also contact us on WhatsApp for any requests:
        <a href="https://wa.me/+919151260784?text=Dear%20Hotel%20Shivaay%20Residency%2C%0A%0ACan%20you%20please%20help%20me%20out%20in%20booking%20hotel%20for%20me%3F%0A%0AThanks%21" target="_top">WhatsApp</a>
      </p>

      <p>
        Please refer to the following <b>Google Map Location</b> as follows,<br><br>
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3562.0205840529015!2d83.07504191521924!3d26.775613783186785!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3991252c9afefd1f%3A0x4e0da2dba2bd9681!2sHotel+Shivaay+Residency!5e0!3m2!1sen!2sin!4v1534758084262" height="100%" width="80%" frameborder="0" style="border:0" allowfullscreen></iframe>
      </p>
    </div>
  </div>
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Hotel Shivaay Residency</h4>
      </div>
      <div class="modal-body">
        <p>Thankyou!</p>
        <p>Your request has been submitted successfully.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>  
  <script>
    $(document).ready(function() {
      $('#feedback').on('submit', function(ev) {
        const submitUrl = $(this).attr('action');
        ev.preventDefault();
        var data = {};
        $($(this).serializeArray()).each(function (index, obj) {
          data[obj.name] = obj.value;
        });
        $.post(submitUrl, data)
          .then(function (resp) {
            if (resp && resp === '1') {
              $('#myModal').modal('show');
            }
            
          })
          .fail(function(err) {
            console.log(err);
          })
      });
    })
  </script>
