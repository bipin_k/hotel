<?php
class Pages extends CI_Controller
{
/*
	public function index($page='home')
	{
		if (!file_exists(APPPATH.'views/pages/'.$page.'.php')) 
		{
			show_404();
		}

		$data['title'] = "Hotel";

		if ($page=='home') 
		{
			$data['title'] = "Hotel";	
		}

		elseif ($page=='contact')
		{
			$data['title']= "Contact";
		}

		$this->load->helper(array('form', 'url'));
		$this->load->view('templates/header', $data);
		$this->load->view('pages/'.$page);
		$this->load->view('templates/footer');
	}*/

	public function feedback_submit()
	{
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		$this->form_validation->set_rules('name', 'Name', 'trim|required|max_length[25]');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|max_length[25]');
		$this->form_validation->set_rules('phone', 'Phone', 'trim|required|exact_length[10]');
		$this->form_validation->set_rules('comment', 'Comment', 'trim|required|max_length[100]');
		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');


		$name= $this->input->post('name');
		$email= $this->input->post('email');
		$phone= $this->input->post('phone');
		$comment= $this->input->post('comment');

		/*$this->load->model('FeedbackModel');
		$this->FeedbackModel->submitFeedback($name, $email, $phone, $comment);
		*/
		 if ($this->form_validation->run())
		 {
		 	$this->load->model('feedbackModel');
		 	if ($this->feedbackModel->submitFeedback($name, $email, $phone, $comment)) 
		 	{
				$this->load->model('FeedbackModel');
				$this->FeedbackModel->submitFeedback($name, $email, $phone, $comment);
		 		echo 1;
		 	}
		 	else
		 	{
		 		echo 0;
		 	}
		 }
		 else 
		 {
		 	echo 0;
		 	// $this->load->helper(array('form', 'url'));
		 	// $this->load->view('templates/header');
		 	// $this->load->view('pages/contact.php');
		 	// $this->load->view('templates/footer');
		 }
	}
}
?>
