<!DOCTYPE html>
<html>
<head>
	<title>Hotel Shivaay Residency</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	
	<style>
	body {
		margin: 0;
		font-family: "Lucida Sans Unicode", "Lucida Grande", sans-serif;
	}

	body{
		background-color: #E2DFD4;
	}

	.title{
		font-size: 50px;
		text-align: center;
		background-color: #292825;
		overflow: hidden;
		padding: 30px;
		color:white;
	}

	.topnav {
		overflow: hidden;
		background-color: #F3BF0E;
		align-items: right;
	}

	.topnav a {
		float: left;
		color: black;
		text-align: center;
		padding: 14px 16px;
		text-decoration: none;
		font-size: 17px;
	}

	.topnav a:hover {
		background-color: #ddd;
		color: black;
	}

	.topnav a.active {
		background-color: #4CAF50;
		color: white;
	}
</style>
</head>
<body>
	<div class="title">Hotel Shivaay Residency</div>
	<div class="topnav">
		<a class="active" href="<?php echo base_url();?>">Home</a>
		<a href="<?php echo base_url();?>news">News</a>
		<a href="<?php echo base_url();?>explore">Explore</a>
		<a href="<?php echo base_url();?>contact">Contact</a>
	</div>	
