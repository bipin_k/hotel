<div class= "container-fluid">
      <br>
      <p>
            <b>Samay Mata Temple</b><br>
            Samay Mata Temple is the one of the oldest and popular temple of Sant Kabir Nagar. It’s situated in middle of Khalilabad City near Kotwali Police Station.<br><br>

            <b>Tameswar Nath Temple</b><br>
            The ancient temple of Tameswar Nath, It believed that the Pandavas mother of the Kunti, was first-time worship of that temple, from that time It situated at that place of Tameswar Nath.<br><br>

            <b>Fort of Qazi Khailil-ur-Rahman</b><br>
            The town is small but has a history which can be traced back to Mughal emperors. The place derives its name from its founder, Qazi Khailil-ur-Rahman, who was appointed chakladar of Gorakhpur about 1860. Khalil-ur-Rahman was sent to suppress revolts by Rajputs from the nearby villages. At present this place is more famous for its handloom cloth market, popularly known as Bardahia Bazar.<br><br>

            <b>Bardhahiya Bazar</b><br>
            Bardahiya Bazar is more famous for its handloom cloth market, popularly known as Bardahia Bazar. The tehsil building, situated to the south of the road to Gorakhpur, is an imposing structure created after the first freedom struggle in 1857 in which the place was sacked.<br><br>

            <b>Bakhira Sanctuary and Bakhira Moti Jheel</b><br>
            The Bakhira Bird Sanctuary is the largest natural flood plain wetland of India in Sant Kabir Nagar district of Eastern Uttar Pradesh. The sanctuary was established in 1980. It is situated 44 km west of Gorakhpur city. It is a vast stretch of water body expanding over an area of 29 km2. This is an important lake of eastern UP, which provides a wintering and staging ground for a number of migratory waterfowls and a breeding ground for resident birds.<br><br>

            <b>Maghar</b><br>
            The enlightened master, Kabir left his body in Maghar in Januanry, 1518, Magh Shukl Ekadashi according to the Hindu calendar in Vikram Samvat 1575. He was loved equally by Muslims and Hindus, and on his death both a mazaar (tomb) and samadhi were built by the Muslims and Hindus respectively. His mazaar and samadhi stand side by side. An annual festival is held here on Makar Sankranti 14th January. Kabir chose Maghar above Kashi because as an enlightened soul he wanted to dispel the myth that anyone breathing his last in Magahar is born a donkey in his next life.<br><br>
      </p>