<?php
class HomeController extends CI_Controller
{
	public function index()
	{
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->view('templates/header');
		$this->load->view('pages/home.php');
		$this->load->view('templates/footer');
	}
}
?>